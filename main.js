"use strict";

class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
    this.render();
  }

  render() {
    const card = document.createElement("div");
    card.classList.add("card");

    const userInfo = document.createElement("div");
    userInfo.classList.add("user-info");

    const name = document.createElement("span");
    name.textContent = `${this.user.name}`;
    userInfo.appendChild(name);

    const email = document.createElement("span");
    email.textContent = `${this.user.email}`;
    userInfo.appendChild(email);

    const editBtn = document.createElement("span");
    editBtn.classList.add("edit-btn");
    editBtn.textContent = "Edit";
    editBtn.addEventListener("click", this.editCard.bind(this));
    userInfo.appendChild(editBtn);

    const deleteBtn = document.createElement("span");
    deleteBtn.classList.add("delete-btn");
    deleteBtn.textContent = "Delete";
    deleteBtn.addEventListener("click", this.deleteCard.bind(this));
    userInfo.appendChild(deleteBtn);

    card.appendChild(userInfo);

    const title = document.createElement("h3");
    title.textContent = this.post.title;
    card.appendChild(title);

    const content = document.createElement("p");
    content.textContent = this.post.body;
    card.appendChild(content);

    this.element = card;
  }

  async editCard() {
    const newTitle = prompt("Enter new title", this.post.title);
    const newText = prompt("Enter new text", this.post.body);
    if (newTitle === null || newText === null) {
      return;
    }
    const response = await fetch(
      `https://ajax.test-danit.com/api/json/posts/${this.post.id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          title: newTitle,
          body: newText,
          userId: this.post.userId,
        }),
      }
    );
    if (response.ok) {
      const data = await response.json();
      this.post.title = data.title;
      this.post.body = data.body;
      const titleElem = this.element.querySelector("h3");
      const contentElem = this.element.querySelector("p");
      titleElem.textContent = data.title;
      contentElem.textContent = data.body;
    } else {
      alert("Failed to update post");
    }
  }

  deleteCard() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
      method: "DELETE",
    }).then((response) => {
      if (response.ok) {
        this.element.remove();
      } else {
        alert("Failed to delete post");
      }
    });
  }
}

async function fetchUsers() {
  const response = await fetch("https://ajax.test-danit.com/api/json/users");
  const data = await response.json();
  return data;
}

async function fetchPosts() {
  const response = await fetch("https://ajax.test-danit.com/api/json/posts");
  const data = await response.json();
  return data;
}

async function renderCards() {
  const [users, posts] = await Promise.all([fetchUsers(), fetchPosts()]);
  const app = document.getElementById("app");

  const loading = document.createElement("div");
  loading.classList.add("loader");
  app.appendChild(loading);

  for (const post of posts) {
    const user = users.find((u) => u.id === post.userId);
    if (!user) {
      console.warn(`Could not find user for post ${post.id}`);
      continue;
    }

    const card = new Card(post, user);
    app.appendChild(card.element);
  }
  app.removeChild(loading);
}

renderCards();

async function createPost() {
  const [user1] = await fetchUsers();
  const title = prompt("Enter title");
  const body = prompt("Enter text");
  if (title === null || body === null) {
    return;
  }
  const response = await fetch("https://ajax.test-danit.com/api/json/posts", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      title: title,
      body: body,
      userId: 1,
    }),
  });
  if (response.ok) {
    const data = await response.json();
    const app = document.getElementById("app");
    const card = new Card(data, {
      name: user1.name,
      email: user1.email,
    });
    app.insertBefore(card.element, app.firstChild);
  }
}

const addPostButton = document.getElementById("add-post");
addPostButton.addEventListener("click", createPost);
